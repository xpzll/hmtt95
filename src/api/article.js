// 以后这个文件封装跟新闻有关的接口
import request from '@/utils/request'

// 封装一个获取文章列表的接口
export const articleListAPI = (params) => {
  return request({
    url: '/v1_0/articles',
    params
  })
}

// 封装一个获取文章详情的接口
export const detailAPI = (id) => {
  return request({
    url: `/v1_0/articles/${id}`
  })
}
