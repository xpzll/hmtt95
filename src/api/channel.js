// 以后这个文件专门封装跟频道有关的接口

import request from '@/utils/request'

// 获取用户自己的频道接口
export const ownListAPI = () => {
  return request({
    url: '/v1_0/user/channels'
  })
}

// 获取所有的频道接口
export const allListAPI = () => {
  return request({
    url: '/v1_0/channels'
  })
}

// 封装重置频道的接口
export const resetChannelAPI = (data) => {
  return request({
    url: '/v1_0/user/channels',
    method: 'put',
    data
  })
}
