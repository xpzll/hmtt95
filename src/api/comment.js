import request from '@/utils/request'

// 封装一个获取评论（或者回复）的接口
export const cmtListAPI = (params) => {
  return request({
    url: '/v1_0/comments',
    params
  })
}

// 封装一个发表评论（或者回复）的接口
export const sendCmtAPI = (data) => {
  return request({
    url: '/v1_0/comments',
    method: 'post',
    data
  })
}
