import request from '@/utils/request'

// 封装一个搜索建议的接口
export const suggestAPI = (params) => {
  return request({
    url: '/v1_0/suggestion',
    params
  })
}

// 封装一个获取搜索结果的接口
export const resultAPI = (params) => {
  return request({
    url: '/v1_0/search',
    params
  })
}
