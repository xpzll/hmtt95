// 导入请求对象
import request from '@/utils/request'

import store from '@/store'

// 封装一个做登录的接口
export const loginAPI = (data) => {
  return request({
    url: '/v1_0/authorizations',
    method: 'post',
    data
  })
}
// 封装一个获取用户信息的接口
export const userInfoAPI = (data) => {
  return request({
    url: '/v1_0/user/profile',
    method: 'get'
  })
}

// 封装一个修改头像的接口
export const avatarAPI = (data) => {
  return request({
    url: '/v1_0/user/photo',
    method: 'patch',
    data
  })
}

// 封装一个修改资料的接口
export const editUserInfoAPI = (data) => {
  return request({
    url: '/v1_0/user/profile',
    method: 'patch',
    data
  })
}
