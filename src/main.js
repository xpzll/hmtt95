import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 导入全局样式
import './styles/base.less'
// 导入vant
import './utils/vant.js'
// 导入flexible
import 'amfe-flexible'

// 导入dayjs
import dayjs from 'dayjs'
// 导入相对时间的插件
import relativeTime from 'dayjs/plugin/relativeTime'
import 'dayjs/locale/zh-cn'
dayjs.locale('zh-cn')
dayjs.extend(relativeTime)

// 封装一个方法并挂载到Vue的原型上
Vue.prototype.$relvTime = (time) => {
  return dayjs(time).fromNow()
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
