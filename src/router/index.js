import Vue from 'vue'
import VueRouter from 'vue-router'

import store from '@/store'
import { Toast } from 'vant'
// 导入组件
// import MyLogin from '@/views/login'
// import MyLayout from '@/views/layout'
// import home from '@/views/home'
// import ask from '@/views/ask'
// import video from '@/views/video'
// import mine from '@/views/mine'
// import UserEdit from '@/views/mine/UserEdit'
// import article from '@/views/article'
// import search from '@/views/search'
// import SearchResult from '@/views/search/SearchResult'
// import MyChat from '@/views/mine/MyChat'

// 以下用懒加载形式
const MyLogin = () => import('@/views/login')
const MyLayout = () => import('@/views/layout')
const home = () => import('@/views/home')
const ask = () => import('@/views/ask')
const video = () => import('@/views/video')
const mine = () => import('@/views/mine')
const UserEdit = () => import('@/views/mine/UserEdit')
const article = () => import('@/views/article')
const search = () => import('@/views/search')
const SearchResult = () => import('@/views/search/SearchResult')
const MyChat = () => import('@/views/mine/MyChat')

Vue.use(VueRouter)

const routes = [
  // 路由重定向
  { path: '/', redirect: '/layout/home' },
  {
    name: 'login',
    path: '/login',
    component: MyLogin
  },
  {
    name: 'article',
    // 这里的:id不是让你写:id，而是传一个id作为参数
    path: '/article/:id',
    component: article
  },
  {
    name: 'MyChat',
    path: '/chat',
    component: MyChat
  },
  {
    name: 'SearchResult',
    path: '/search/result',
    component: SearchResult
  },
  {
    name: 'search',
    // 这里的:id不是让你写:id，而是传一个id作为参数
    path: '/search',
    component: search
  },
  {
    name: 'edit',
    path: '/mine/edit',
    component: UserEdit
  },
  {
    name: 'layout',
    path: '/layout',
    component: MyLayout,
    children: [
      {
        name: 'home',
        path: 'home',
        component: home,
        meta: {
          needKeep: true
        }
      },
      {
        name: 'ask',
        path: 'ask',
        component: ask
      },
      {
        name: 'video',
        path: 'video',
        component: video
      },
      {
        name: 'mine',
        path: 'mine',
        component: mine,
        // 路由元信息，代表把一些数据存到路由对象上
        // 或者换句话说相当于给这个打标记
        meta: {
          needLogin: true
        }
      }
    ]
  }
]

const router = new VueRouter({
  routes
})
// 全局前置守卫
// to：   去哪（路由对象）
// from： 从哪来（路由对象）
// next： 是一个函数，调用它代表放行，不调用就是不放行
router.beforeEach((to, from, next) => {
  // 判断要去的(to)是不是需要登录的页面
  if (to.meta.needLogin) {
    // 代表是需要登录的页面
    // 判断有没有token
    if (store.state.user.tokenObj.token) {
      // 登录了，直接放行
      next()
    } else {
      // 没登录，打回登录页
      Toast('请先登录')
      next({
        name: 'login',
        query: {
          // 原本想去哪就记录去哪的路径
          back: to.path
        }
      })
    }
  } else {
    // 不需要登录的页面，直接放行
    next()
  }
})

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}
export default router
