import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({

  modules: {
    user
  },
  plugins: [createPersistedState({
    // key改为hmtt95
    key: 'hmtt95',
    // 我只要持久化user模块里的tokenObj
    paths: ['user.tokenObj']
  })]
})
