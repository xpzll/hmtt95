import { loginAPI, userInfoAPI } from '@/api/user'
import { Toast } from 'vant'
import router from '@/router'
export default {
  // 开启命名空间
  namespaced: true,
  state () {
    return {
      // 保存token，这是一个对象，里面有refresh_token和token
      tokenObj: {},
      // 用来保存获取的用户信息
      userInfo: {}
    }
  },
  mutations: {
    setToken (state, obj) {
      state.tokenObj = obj
    },
    // 修改userInfo的方法
    setUserInfo (state, obj) {
      state.userInfo = obj
    },

    logout (state) {
      // 清空token
      state.tokenObj = {}
      // 清空用户信息
      state.userInfo = {}
      router.push('/login')
    }
  },
  actions: {
    async login (ctx, info) {
      try {
        // 直接在这写代码 - 因为当这个函数被调用，证明一定校验通过了
        // 所以可以发登录的请求
        const res = await loginAPI(info)
        ctx.commit('setToken', res.data)
        Toast.success('登录成功')
        // 跳转，取一下有没有参数
        if (info.back) {
          router.push(info.back)
        } else {
          // 调回首页
          router.push('/layout/home')
        }
      } catch {
        Toast.fail('登录失败')
      }
    },

    // 发请求获取用户信息
    async reqUserInfo (ctx) {
      // 没数据才发请求
      if (!ctx.state.userInfo.id) {
        const res = await userInfoAPI()
        ctx.commit('setUserInfo', res.data)
      }
    }
  }
}
