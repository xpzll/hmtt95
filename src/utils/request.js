// 导入axios
import axios from 'axios'
import store from '@/store'
import router from '@/router'
import { Toast } from 'vant'

// 设置基地址
const request = axios.create({
  // baseURL: 'http://geek.itheima.net/'
  baseURL: 'http://toutiao.itheima.net/'
})
// 添加请求拦截器
request.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    // 判断有没有token，有才加
    const token = store.state.user.tokenObj.token
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }

    return config
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 添加响应拦截器
request.interceptors.response.use(
  function (response) {
    // 做数据剥离
    return response.data
  },
  async function (error) {
    // 当这个函数被调用就证明响应出错了，要利用refresh_token去得到一个新的token
    // 但是要是401错误，才需要去刷新token，别的错误不用管
    if (error.response.status === 401) {
      try {
        // 用axios发请求，不然会出错
        const res = await axios({
          url: 'http://geek.itheima.net/v1_0/authorizations',
          method: 'put',
          headers: {
            Authorization: `Bearer ${store.state.user.tokenObj.refresh_token}`
          }
        })
        // 得到最新token存起来，vuex里
        store.commit('user/setToken', {
          token: res.data.data.token,
          // 因为服务器只会给你返回最新token，而我们应该存两个，既要有token也要有refresh_token
          // 所以还得自己多加一个refresh_token就用之前的值
          refresh_token: store.state.user.tokenObj.refresh_token
        })

        // 已经有新的token了，那么再把用户之前想发的请求再发一次
        // 把结果return
        return request(error.config)
      } catch {
        // 进到catch代表连refresh_token都过期了，要打回登录页
        Toast('登录状态失效，请重新登录')
        router.push('/login')
      }
    } else {
      return Promise.reject(error)
    }
  }
)

export default request
