import Vue from 'vue'
import { Button, Cell, CellGroup, NavBar, Form, Field, Toast, Tabbar, TabbarItem, Grid, GridItem, Image as VanImage, Uploader, Dialog, Popup, DatetimePicker, Tab, Tabs, Icon, List, PullRefresh, ActionSheet, Divider, Search } from 'vant'

Vue.use(Search)
Vue.use(Divider)
Vue.use(ActionSheet)
Vue.use(PullRefresh)
Vue.use(List)
Vue.use(Icon)
Vue.use(Tab)
Vue.use(Tabs)
Vue.use(DatetimePicker)
Vue.use(Popup)
// 全局注册
Vue.use(Dialog)
Vue.use(Uploader)
Vue.use(VanImage)
Vue.use(Tabbar)
Vue.use(TabbarItem)
Vue.use(Toast)
Vue.use(Form)
Vue.use(Field)
Vue.use(NavBar)
Vue.use(Cell)
Vue.use(CellGroup)
Vue.use(Button)

Vue.use(Grid)
Vue.use(GridItem)
